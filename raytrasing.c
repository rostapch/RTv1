/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytrasing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:20:52 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:20:53 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

int			first_intersect(t_scene scene, double dist[2])
{
	t_vect	t;
	double	tmp[4];
	int		ret[2];

	dist[1] = 2147483000;
	tmp[0] = scene.flag[0] > 0 ? I_SPHERE : dist[1];
	tmp[1] = scene.flag[1] > 0 ? I_PLAIN : dist[1];
	tmp[2] = scene.flag[2] > 0 ? I_CYLINDER : dist[1];
	tmp[3] = scene.flag[3] > 0 ? I_CONE : dist[1];
	ret[1] = -1;
	ret[0] = -1;
	while (++ret[1] < 4)
	{
		t = vect_number(&scene.camera.look.dir, tmp[ret[1]]);
		if (tmp[ret[1]] >= 0 && (dist[0] = vect_len(&t)) < dist[1])
		{
			dist[1] = dist[0];
			ret[0] = ret[1];
		}
	}
	dist[0] = ret[0] != -1 ? tmp[ret[0]] : -1;
	return (ret[0]);
}

int			intersect(t_scene scene)
{
	double		intns;
	int			rgb[3];
	double		dist[2];
	t_vect		temp[3];

	intns = first_intersect(scene, dist);
	if (dist[0] > 0)
	{
		POINT = vect_number(&scene.camera.look.dir, dist[0]);
		POINT = sum_vect(&POINT, &scene.camera.look.start);
		dist[1] = vect_len(&POINT);
		R_RAY = sub_vect(&scene.light.position, &POINT);
		intns = CHECK_ALL_INTERSEPTIONS;
		intns = shadow(scene, temp, intns);
		return (ft_rgb(rgb[0] * intns, rgb[1] * intns, rgb[2] * intns));
	}
	return (0);
}
