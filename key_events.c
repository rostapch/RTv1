/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:10:00 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:10:02 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

int		key_press(int key)
{
	if (key == 53)
	{
		mlx_destroy_window(g_mw.mlx, g_mw.wnd);
		exit(0);
	}
	else if (key == KEY_UP)
		g_scene.camera.pitch += 10;
	else if (key == KEY_DOWN)
		g_scene.camera.pitch -= 10;
	else if (key == KEY_LEFT)
		g_scene.camera.yaw += 10;
	else if (key == KEY_RIGHT)
		g_scene.camera.yaw -= 10;
	if (ANY_KEY)
		scene_count();
	return (0);
}

int		exit_x(void *par)
{
	(void)par;
	return (key_press(53));
}

int		m_key_press(int button, int x, int y, void *param)
{
	t_vect	move;

	(void)param;
	x = y;
	move = vect_number(&g_scene.camera.look.dir, 150);
	if (button == MW_UP)
		g_scene.camera.look.start = sum_vect(&g_scene.camera.look.start, &move);
	if (button == MW_DOWN)
		g_scene.camera.look.start = sub_vect(&g_scene.camera.look.start, &move);
	if (ANY_BUTTON)
		scene_count();
	return (button);
}
