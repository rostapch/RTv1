/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   materials.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 16:10:29 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 16:10:32 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void	init_materials(void)
{
	g_rubber.ka = 0.02;
	g_rubber.kd = 0.01;
	g_rubber.ks = 0.4;
	g_rubber.l = 10;
	g_plastic.ka = 0.16;
	g_plastic.kd = 0.7;
	g_plastic.ks = 0.21;
	g_plastic.l = 9;
	g_matte.ka = 0.1522;
	g_matte.kd = 0.4321;
	g_matte.ks = 0.2082;
	g_matte.l = 2;
	g_silver.ka = 0.19225;
	g_silver.kd = 0.60754;
	g_silver.ks = 0.5082;
	g_silver.l = 8;
	g_polished_silver.ka = 0.19225;
	g_polished_silver.kd = 0.60754;
	g_polished_silver.ks = 0.7782;
	g_polished_silver.l = 24;
	g_chrome.ka = 0.25;
	g_chrome.kd = 0.47;
	g_chrome.ks = 0.32;
	g_chrome.l = 76;
}

void	color_bronze(t_material *material)
{
	material->r = 180;
	material->g = 90;
	material->b = 0;
}

void	color_chrome(t_material *material)
{
	material->r = 227;
	material->g = 222;
	material->b = 219;
}

void	color_silver(t_material *material)
{
	material->r = 237;
	material->g = 242;
	material->b = 249;
}

void	color_dark_oak(t_material *material)
{
	material->r = 90;
	material->g = 65;
	material->b = 57;
}
