/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersections.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:13:19 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:13:22 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

double		ray_sphere(t_ray *r, t_sphere *s)
{
	double	abcd[4];
	double	t[2];
	t_vect	distance;

	A = DOT(&r->dir, &r->dir);
	distance = sub_vect(&r->start, &s->centre);
	B = 2.0 * DOT(&r->dir, &distance);
	C = DOT(&distance, &distance) - s->R * s->R;
	D = B * B - 4.0 * A * C;
	if (D >= 0)
	{
		t[0] = (-B - sqrt(D)) / (2 * A);
		t[1] = (-B + sqrt(D)) / (2 * A);
		if ((t[0] = t[1] < t[0] && t[1] > 0 ? t[1] : t[0]) >= 0)
			return (t[0]);
	}
	return (-1);
}

double		ray_plain(t_ray *r, t_plain *p)
{
	double	t;
	t_vect	l_temp;

	l_temp = sub_vect(&p->point, &r->start);
	t = DOT(&l_temp, &p->normal);
	t = t / (DOT(&r->dir, &p->normal));
	if (t >= 0 && t < ROD)
		return (t);
	return (-1);
}

double		ray_cylinder(t_ray *r, t_cylinder *c)
{
	t_vect	l_temp[4];
	double	abcd[4];
	double	t[2];

	l_temp[0] = sub_vect(&r->start, &c->point);
	l_temp[1] = cross_vect(&l_temp[0], &c->dir);
	l_temp[2] = cross_vect(&r->dir, &c->dir);
	A = DOT(&l_temp[2], &l_temp[2]);
	B = 2 * DOT(&l_temp[2], &l_temp[1]);
	C = DOT(&l_temp[1], &l_temp[1]) - c->R * c->R * DOT(&c->dir, &c->dir);
	D = B * B - 4.0 * A * C;
	if (D >= 0)
	{
		t[0] = (-B - sqrt(D)) / (2 * A);
		t[1] = (-B + sqrt(D)) / (2 * A);
		if ((t[0] = t[1] < t[0] && t[1] > 0 ? t[1] : t[0]) >= 0)
			return (t[0]);
	}
	return (-1);
}

double		ray_cone(t_ray *r, t_cone *c)
{
	double	t[3];
	double	abcd[4];
	t_vect	l_temp[3];

	l_temp[0] = sub_vect(&r->start, &c->point);
	t[0] = DOT(&r->dir, &c->dir);
	l_temp[1] = vect_number(&c->dir, t[0]);
	l_temp[1] = sub_vect(&r->dir, &l_temp[1]);
	t[1] = DOT(&l_temp[0], &c->dir);
	l_temp[2] = vect_number(&c->dir, t[1]);
	l_temp[2] = sub_vect(&l_temp[0], &l_temp[2]);
	A = COS_F * DOT(&l_temp[1], &l_temp[1]) - SIN_F * t[0] * t[0];
	B = 2 * (COS_F * DOT(&l_temp[1], &l_temp[2]) - SIN_F * t[0] * t[1]);
	C = COS_F * DOT(&l_temp[2], &l_temp[2]) - SIN_F * t[1] * t[1];
	D = B * B - 4.0 * A * C;
	if (D >= 0)
	{
		t[0] = (-B - sqrt(D)) / (2 * A);
		t[1] = (-B + sqrt(D)) / (2 * A);
		if ((t[0] = t[1] < t[0] && t[1] > 0 ? t[1] : t[0]) >= 0)
			return (t[0]);
	}
	return (-1);
}
