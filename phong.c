/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phong.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:22:21 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:22:22 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

double		phong(t_scene scene, t_vect temp[3], double k[4], double dist)
{
	double	intns;
	t_vect	h_norm[2];
	t_vect	l_temp;

	V = sub_vect(&scene.camera.look.start, &POINT);
	V = normalize(&V);
	L = vect_number(&NORM, 2 * DOT(&NORM, &R_RAY));
	L = sub_vect(&L, &R_RAY);
	L = vect_number(&L, 1.0 / vect_len(&L));
	l_temp = vect_number(&R_RAY, 1.0 / vect_len(&R_RAY));
	intns = k[0] * scene.light.ia
	+ k[1] * (DOT(&NORM, &l_temp)) * scene.light.id
	+ k[2] * pow(DOT(&V, &L), k[3]) * scene.light.is;
	intns = fmin(fmax(intns * (1.0 - dist / ROD), 0), 1);
	return (intns);
}

double		shadow(t_scene scene, t_vect temp[3], double intns)
{
	t_ray	reflected;
	double	dist[3];
	double	min;

	reflected.dir = normalize(&R_RAY);
	reflected.start = vect_number(&NORM, 0.01);
	reflected.start = sum_vect(&reflected.start, &POINT);
	dist[0] = scene.flag[0] > 0 ? SH_SPHERE : -1;
	dist[1] = scene.flag[2] > 0 ? SH_CYLINDER : -1;
	dist[2] = scene.flag[3] > 0 ? SH_CONE : -1;
	if ((dist[0]) > 0 || (dist[1]) > 0 || dist[2] > 0)
	{
		min = fmax(fmax(dist[0], dist[1]), dist[2]);
		if (dist[0] < min && dist[0] > 0)
			min = dist[0];
		else if (dist[1] < min && dist[1] > 0)
			min = dist[1];
		else if (dist[2] < min && dist[2] > 0)
			min = dist[2];
		dist[0] = min;
		POINT = vect_number(&reflected.dir, dist[0]);
		if (vect_len(&POINT) <= vect_len(&R_RAY) && vect_len(&POINT) >= 0.001)
			intns *= 0.6;
	}
	return (intns);
}
