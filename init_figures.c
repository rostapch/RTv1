/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_figures.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:23:17 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:23:19 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void		init_sphere(t_scene *scene)
{
	scene->flag[0] = 1;
	scene->sphere.material = g_silver;
	color_silver(&scene->sphere.material);
	scene->sphere.centre.x = 500;
	scene->sphere.centre.y = 0;
	scene->sphere.centre.z = 0;
	scene->sphere.R = 60;
}

void		init_plain(t_scene *scene)
{
	scene->flag[1] = 1;
	scene->plain.material = g_chrome;
	color_chrome(&scene->plain.material);
	scene->plain.point.x = 0;
	scene->plain.point.y = -140;
	scene->plain.point.z = 0;
	scene->plain.normal.x = 0;
	scene->plain.normal.y = 1;
	scene->plain.normal.z = 0;
}

void		init_cylinder(t_scene *scene)
{
	scene->flag[2] = 1;
	scene->cylinder.material = g_plastic;
	scene->cylinder.material.r = 150;
	scene->cylinder.material.g = 250;
	scene->cylinder.material.b = 180;
	scene->cylinder.point.x = -400;
	scene->cylinder.point.y = 0;
	scene->cylinder.point.z = 410;
	scene->cylinder.dir.x = -1;
	scene->cylinder.dir.y = -1;
	scene->cylinder.dir.z = 0;
	scene->cylinder.R = 20;
}

void		init_cone(t_scene *scene)
{
	scene->flag[3] = 1;
	scene->cone.material = g_matte;
	color_dark_oak(&scene->cone.material);
	scene->cone.point.x = -300;
	scene->cone.point.y = 0;
	scene->cone.point.z = 0;
	scene->cone.dir.x = 0;
	scene->cone.dir.y = 1;
	scene->cone.dir.z = 0;
	scene->cone.R = 10;
}
