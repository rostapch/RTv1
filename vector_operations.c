/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:11:11 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:11:29 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

t_vect	sum_vect(t_vect *v1, t_vect *v2)
{
	t_vect	ret;

	ret.x = v1->x + v2->x;
	ret.y = v1->y + v2->y;
	ret.z = v1->z + v2->z;
	return (ret);
}

t_vect	sub_vect(t_vect *v1, t_vect *v2)
{
	t_vect	ret;

	ret.x = v1->x - v2->x;
	ret.y = v1->y - v2->y;
	ret.z = v1->z - v2->z;
	return (ret);
}

double	dot_vect(t_vect *v1, t_vect *v2)
{
	double ret;

	ret = v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
	return (ret);
}

t_vect	vect_number(t_vect *v, double val)
{
	t_vect	ret;

	ret.x = v->x * val;
	ret.y = v->y * val;
	ret.z = v->z * val;
	return (ret);
}

t_vect	cross_vect(t_vect *u, t_vect *v)
{
	t_vect	ret;

	ret.x = u->y * v->z - u->z * v->y;
	ret.y = u->z * v->x - u->x * v->z;
	ret.z = u->x * v->y - u->y * v->x;
	return (ret);
}
