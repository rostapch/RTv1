/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 15:12:01 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/09 15:12:05 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void			count_direction(int x, int y)
{
	double		xy[2];
	int			color;

	xy[1] = (1 - (2.0 * ((y + 0.5)) / W_H)) * TANFOV;
	xy[0] = ((2.0 * ((x + 0.5)) / (W_W)) - 1) * TANFOV * ((double)W_W / W_H);
	g_scene.camera.look.dir.y = sin(PITCH_D) + xy[1] * cos(PITCH_D);
	g_scene.camera.look.dir.x = -cos(PITCH_D) * sin(YAW_D) +
	xy[0] * cos(YAW_D) + xy[1] * sin(PITCH_D) * sin(YAW_D);
	g_scene.camera.look.dir.z = cos(PITCH_D) * cos(YAW_D) +
	xy[0] * sin(YAW_D) - xy[1] * sin(PITCH_D) * cos(YAW_D);
	normalize(&g_scene.camera.look.dir);
	if ((color = intersect(g_scene)) > 0)
		mlx_pixel_put(g_mw.mlx, g_mw.wnd, x, y, color);
}

void			scene_count(void)
{
	double		dirs[3];
	int			xy[2];

	dirs[0] = g_scene.camera.look.dir.y;
	dirs[1] = g_scene.camera.look.dir.x;
	dirs[2] = g_scene.camera.look.dir.z;
	g_scene.cylinder.dir = normalize(&g_scene.cylinder.dir);
	g_scene.cone.dir = normalize(&g_scene.cone.dir);
	g_scene.plain.normal = normalize(&g_scene.plain.normal);
	mlx_clear_window(g_mw.mlx, g_mw.wnd);
	xy[1] = -1;
	while (++xy[1] < W_H)
	{
		xy[0] = -1;
		while (++xy[0] < W_W)
			count_direction(xy[0], xy[1]);
	}
	g_scene.camera.look.dir.y = dirs[0];
	g_scene.camera.look.dir.x = dirs[1];
	g_scene.camera.look.dir.z = dirs[2];
}

int				main(void)
{
	g_scene.flag[0] = 0;
	g_scene.flag[1] = 0;
	g_scene.flag[2] = 0;
	g_scene.flag[3] = 0;
	g_scene.flag[4] = 0;
	init_materials();
	init_scene(&g_scene);
	g_mw.mlx = mlx_init();
	g_mw.wnd = mlx_new_window(g_mw.mlx, W_W, W_H, "RTv1");
	mlx_key_hook(g_mw.wnd, key_press, g_mw.mlx);
	mlx_hook(g_mw.wnd, 4, 1L << 0, m_key_press, g_mw.mlx);
	mlx_hook(g_mw.wnd, 17, 1L << 17, exit_x, g_mw.mlx);
	scene_count();
	mlx_loop(g_mw.mlx);
}
