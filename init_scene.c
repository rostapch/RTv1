/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_scene.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:24:09 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:24:10 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void		init_light(t_scene *scene)
{
	scene->flag[4] = 1;
	scene->light.position.x = -700;
	scene->light.position.y = 500;
	scene->light.position.z = 0;
	scene->light.ia = 0.85;
	scene->light.id = 0.4;
	scene->light.is = 0.8;
}

void		init_scene(t_scene *scene)
{
	init_sphere(scene);
	init_plain(scene);
	init_cylinder(scene);
	init_cone(scene);
	init_light(scene);
	scene->camera.pitch = 0;
	scene->camera.yaw = 180;
	scene->camera.look.dir.z = -1;
	scene->camera.look.start.x = -200;
	scene->camera.look.start.y = 0;
	scene->camera.look.start.z = 1500;
}
