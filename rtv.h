/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:43:17 by rostapch          #+#    #+#             */
/*   Updated: 2017/08/14 15:43:19 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV_H
# define RTV_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include "minilibx_macos_2016/mlx.h"
# include "libft/libft.h"
# include "math.h"

# define W_H 1200
# define W_W 1600

# define PI 3.141592653589793

# define FOV 45
# define PITCH_D (g_scene.camera.pitch * PI / 180)
# define YAW_D (g_scene.camera.yaw * PI / 180)

# define TANFOV tan(FOV * PI / 180)
# define COS_F pow(cos(c->radius * PI / 180), 2)
# define SIN_F pow(sin(c->radius * PI / 180), 2)

# define ROD 90000

# define KEY_LEFT		123
# define KEY_RIGHT		124
# define KEY_UP			126
# define KEY_DOWN		125

# define R_MB			2
# define L_MB			1
# define MW_UP			4
# define MW_DOWN		5
# define ANY_KEY (key == 123 || key == 124 || key == 125 || key == 126)
# define ANY_BUTTON		(button == MW_UP || button == MW_DOWN)

# define A abcd[0]
# define B abcd[1]
# define C abcd[2]
# define D abcd[3]

# define DOT dot_vect

# define R radius
# define V h_norm[0]
# define L h_norm[1]

# define POINT temp[0]
# define NORM temp[1]
# define R_RAY temp[2]

# define DISTANCE vect_len(&temp[2]) + vect_len(&temp[0])

# define I_SPHERE ray_sphere(&scene.camera.look, &scene.sphere)
# define I_CYLINDER ray_cylinder(&scene.camera.look, &scene.cylinder)
# define I_CONE ray_cone(&scene.camera.look, &scene.cone)
# define I_PLAIN ray_plain(&scene.camera.look, &scene.plain)

# define SH_SPHERE ray_sphere(&reflected, &scene.sphere)
# define SH_CYLINDER ray_cylinder(&reflected, &scene.cylinder)
# define SH_CONE ray_cone(&reflected, &scene.cone)

# define C_SP ((int)intns == 0 && scene.flag[0] > 0)
# define S_SP (sphere_count(scene, temp, rgb))
# define C_PL ((int)intns == 1 && scene.flag[1] > 0)
# define S_PL (plain_count(scene, temp, rgb))
# define C_CY ((int)intns == 2 && scene.flag[2] > 0)
# define S_CY (cylinder_count(scene, temp, rgb))
# define SC_CO (scene.flag[3] > 0 ? cone_count(scene, temp, rgb) : 0)
# define CC_CHECK ((C_CY) ? (S_CY) : (SC_CO))
# define CHECK_ALL_INTERSEPTIONS (C_SP) ? (S_SP) : (C_PL) ? (S_PL) : (CC_CHECK)

typedef struct			s_mw
{
	void				*mlx;
	void				*wnd;
}						t_mw;

typedef struct			s_vect
{
	double				x;
	double				y;
	double				z;
}						t_vect;

typedef struct			s_light
{
	t_vect				position;
	double				ia;
	double				id;
	double				is;
}						t_light;

typedef struct			s_material
{
	double				ka;
	double				kd;
	double				ks;
	double				l;
	double				r;
	double				g;
	double				b;
}						t_material;

typedef struct			s_sphere
{
	t_material			material;
	t_vect				centre;
	double				radius;
}						t_sphere;

typedef struct			s_plain
{
	t_material			material;
	t_vect				point;
	t_vect				normal;
}						t_plain;

typedef struct			s_cone
{
	t_material			material;
	t_vect				point;
	t_vect				dir;
	double				radius;
}						t_cone;

typedef struct			s_cylinder
{
	t_material			material;
	t_vect				point;
	t_vect				dir;
	double				radius;
}						t_cylinder;

typedef struct			s_ray
{
	t_vect				start;
	t_vect				dir;
}						t_ray;

typedef struct			s_cam
{
	t_ray				look;
	double				pitch;
	double				yaw;
}						t_cam;

typedef struct			s_scene
{
	int					flag[5];
	t_sphere			sphere;
	t_cylinder			cylinder;
	t_plain				plain;
	t_cone				cone;
	t_cam				camera;
	t_light				light;
}						t_scene;

t_mw					g_mw;
t_scene					g_scene;

t_material				g_rubber;
t_material				g_plastic;
t_material				g_matte;
t_material				g_silver;
t_material				g_polished_silver;
t_material				g_chrome;

double					vect_len(t_vect *v);
t_vect					normalize(t_vect *v);
t_vect					sum_vect(t_vect *v1, t_vect *v2);
t_vect					sub_vect(t_vect *v1, t_vect *v2);
double					mult_vect(t_vect *v1, t_vect *v2);
t_vect					vect_number(t_vect *v, double val);
double					dot_vect(t_vect *v1, t_vect *v2);
t_vect					cross_vect(t_vect *u, t_vect *v);

double					ray_sphere(t_ray *r, t_sphere *s);
double					ray_plain(t_ray *r, t_plain *p);
double					ray_cylinder(t_ray *r, t_cylinder *c);
double					ray_cone(t_ray *r, t_cone *c);

double					phong(t_scene scene, t_vect temp[3],
						double k[4], double dist);
double					plain_count(t_scene scene, t_vect temp[3], int rgb[3]);
double					sphere_count(t_scene scene, t_vect temp[3], int rgb[3]);
double					cylinder_count(t_scene scene, t_vect temp[3],
						int rgb[3]);
double					cone_count(t_scene scene, t_vect temp[3], int rgb[3]);

int						first_intersect(t_scene scene, double dist[2]);
double					shadow(t_scene scene, t_vect temp[3], double intns);
int						intersect(t_scene scene);

void					init_sphere(t_scene *scene);
void					init_plain(t_scene *scene);
void					init_cylinder(t_scene *scene);
void					init_cone(t_scene *scene);
void					init_scene(t_scene *scene);
void					scene_count();

int						key_press(int key);
int						exit_x(void *par);
int						m_key_press(int button, int x, int y, void *param);

void					init_materials();
void					color_bronze(t_material *material);
void					color_chrome(t_material *material);
void					color_silver(t_material *material);
void					color_dark_oak(t_material *material);

#endif
