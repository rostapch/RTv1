# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rostapch <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/21 16:14:05 by rostapch          #+#    #+#              #
#    Updated: 2017/10/21 16:14:07 by rostapch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

NAME = RTv1

FRAMEFLAGS = -framework OpenGL -framework AppKit

INCLUDE_LIB = -L libft -l ft -L minilibx_macos_2016 -l mlx

HEADERS =  -I . -I libft

SRC_C =	init_figures.c\
		init_materials.c\
		init_scene.c\
		intercections.c\
		key_events.c\
		main.c\
		normal_count.c\
		phong.c\
		raytrasing.c\
		vector_counts.c\
		vector_operations.c

SRC_O =	init_figures.o\
		init_materials.o\
		init_scene.o\
		intercections.o\
		key_events.o\
		main.o\
		normal_count.o\
		phong.o\
		raytrasing.o\
		vector_counts.o\
		vector_operations.o		

OBJ = $(SRC_C:.c=.o)

all: libx lib $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)
	
$(NAME): all

%.o: %.c
	$(CC) -c $(CFLAGS) $(HEADERS)  -o $@ $<

lib:
	make -C libft -f Makefile

lib_re:
	make re -C libft -f Makefile

lib_clean:
	make clean -C libft -f Makefile

lib_fclean:
	make fclean -C libft -f Makefile



libx:
	make -C minilibx_macos_2016 -f Makefile

libx_clean:
	make clean -C minilibx_macos_2016 -f Makefile

libx_re:
	make re -C minilibx_macos_2016 -f Makefile



clean_fdf:
	/bin/rm -f $(SRC_O)

fclean_fdf: clean_fdf
	/bin/rm -f $(NAME)

clean: lib_clean libx_clean
	/bin/rm -f $(SRC_O)



fclean: lib_fclean clean
	/bin/rm -f $(NAME)

re: fclean_fdf lib_re libx_re $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(SRC_O) $(FRAMEFLAGS) $(INCLUDE_LIB) $(HEADERS)
#all:
#	gcc *.c -framework OpenGL -framework AppKit -I . -L libft -l ft -L minilibx_macos_2016 -l mlx -o RTv1
