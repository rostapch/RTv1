/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_count.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rostapch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 17:19:02 by rostapch          #+#    #+#             */
/*   Updated: 2017/10/20 17:19:05 by rostapch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

double		plain_count(t_scene scene, t_vect temp[3], int rgb[3])
{
	double	k[4];

	NORM = normalize(&scene.plain.normal);
	k[0] = scene.plain.material.ka;
	k[1] = scene.plain.material.kd;
	k[2] = scene.plain.material.ks;
	k[3] = scene.plain.material.l;
	rgb[0] = scene.plain.material.r;
	rgb[1] = scene.plain.material.g;
	rgb[2] = scene.plain.material.b;
	return (phong(scene, temp, k, DISTANCE));
}

double		sphere_count(t_scene scene, t_vect temp[3], int rgb[3])
{
	double	k[4];

	NORM = sub_vect(&POINT, &scene.sphere.centre);
	NORM = normalize(&NORM);
	k[0] = scene.sphere.material.ka;
	k[1] = scene.sphere.material.kd;
	k[2] = scene.sphere.material.ks;
	k[3] = scene.sphere.material.l;
	rgb[0] = scene.sphere.material.r;
	rgb[1] = scene.sphere.material.g;
	rgb[2] = scene.sphere.material.b;
	return (phong(scene, temp, k, DISTANCE));
}

double		cylinder_count(t_scene scene, t_vect temp[3], int rgb[3])
{
	double	k[4];
	t_vect	t;

	t = sub_vect(&scene.cylinder.point, &POINT);
	NORM = vect_number(&scene.cylinder.dir, DOT(&scene.cylinder.dir, &t));
	t = sub_vect(&POINT, &scene.cylinder.point);
	NORM = sum_vect(&NORM, &t);
	NORM = normalize(&NORM);
	k[0] = scene.cylinder.material.ka;
	k[1] = scene.cylinder.material.kd;
	k[2] = scene.cylinder.material.ks;
	k[3] = scene.cylinder.material.l;
	rgb[0] = scene.cylinder.material.r;
	rgb[1] = scene.cylinder.material.g;
	rgb[2] = scene.cylinder.material.b;
	return (phong(scene, temp, k, DISTANCE));
}

double		cone_count(t_scene scene, t_vect temp[3], int rgb[3])
{
	double	k[4];
	t_vect	t;

	NORM = sub_vect(&POINT, &scene.cone.point);
	t = vect_number(&scene.cone.dir, DOT(&scene.cone.dir, &NORM));
	NORM = sub_vect(&NORM, &t);
	NORM = normalize(&NORM);
	k[0] = scene.cone.material.ka;
	k[1] = scene.cone.material.kd;
	k[2] = scene.cone.material.ks;
	k[3] = scene.cone.material.l;
	rgb[0] = scene.cone.material.r;
	rgb[1] = scene.cone.material.g;
	rgb[2] = scene.cone.material.b;
	return (phong(scene, temp, k, DISTANCE));
}
